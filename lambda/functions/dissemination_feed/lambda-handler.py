import boto3
import logging
import os
from collections import namedtuple

s3 = boto3.client('s3')
sqs = boto3.client('sqs')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handler(event, context):

    # retrieve bucket name and file_key from the S3 event
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_key = event['Records'][0]['s3']['object']['key']
    logger.info('Reading {} from {}'.format(file_key, bucket_name))

    obj = s3.get_object(Bucket=bucket_name, Key=file_key)
    lines = obj['Body'].read().decode('utf-8').split('\n')

    Filing = namedtuple(
        "Filing",
        ["cik", "company_name", "form_type", "date_filed", "filename"],
    )

    # build list of 13F filing data
    filings_13f = []
    for line in lines:
        if ".txt" in line:
            data = line.split("|")
            filing = Filing(*data)
            if "13F" in filing.form_type:
                filings_13f.append(filing)

    # build sqs messages to send in batches
    sqs_messages = [
        {
            "Id": f"filing_{filing_13f.cik}_{idx}",
            "MessageBody": "filing",
            "MessageAttributes": {
                "cik": {"StringValue": filing_13f.cik, "DataType": "String"},
                "company_name": {
                    "StringValue": filing_13f.company_name,
                    "DataType": "String",
                },
                "form_type": {
                    "StringValue": filing_13f.form_type,
                    "DataType": "String",
                },
                "date_filed": {
                    "StringValue": filing_13f.date_filed,
                    "DataType": "String",
                },
                "filename": {
                    "StringValue": filing_13f.filename,
                    "DataType": "String",
                },
            },
        }
        for idx, filing_13f in enumerate(filings_13f)
    ]

    message_count = len(sqs_messages)
    idx = 0

    # sqs messages can only be batched in groups of 10
    while idx <= message_count:
        sqs.send_message_batch(
            QueueUrl=os.environ["QUEUE_URL"],
            Entries=sqs_messages[idx : idx + 10],
        )
        idx += 10

    return {'statusCode': 200, 'body': event}

