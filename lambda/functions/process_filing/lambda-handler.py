from collections import namedtuple
import re
import json
import os
import boto3
import logging
import xml.etree.ElementTree as ET

XML_SCHEMA = '{http://www.sec.gov/edgar/document/thirteenf/informationtable}'
s3 = boto3.client('s3')
firehose = boto3.client('firehose')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handler(event, context):

    # retrieve bucket name and file_key from the S3 event
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_key = event['Records'][0]['s3']['object']['key']
    logger.info('Reading {} from {}'.format(file_key, bucket_name))

    obj = s3.get_object(Bucket=bucket_name, Key=file_key)
    xml_data = obj['Body'].read().decode('utf-8')

    # python's xml module can't parse the SEC filing's format,
    # so we use regex to pull out the two XML sections for
    # edgar_submission and information_table
    matches = re.findall(r"<XML>[\s\S]*?<\/XML>", xml_data)
    xml_entities = [x[5:-6].replace("\n", "") for x in matches]
    assert len(xml_entities) == 2

    edgar_submission = ET.fromstring(xml_entities[0])
    information_table = ET.fromstring(xml_entities[1])

    Holding = namedtuple(
        "Holding",
        [
            "nameOfIssuer",
            "titleOfClass",
            "cusip",
            "value",
            "sshPrnamt",
            "sshPrnamtType",
            "investmentDiscretion",
            "putCall",
            "otherManager",
            "Sole",
            "Shared",
            "None_",
        ],
    )

    holdings = []
    for infoTable in information_table.findall(f"{XML_SCHEMA}infoTable"):
        nameOfIssuer = infoTable.find(f"{XML_SCHEMA}nameOfIssuer").text
        titleOfClass = infoTable.find(f"{XML_SCHEMA}titleOfClass").text
        cusip = infoTable.find(f"{XML_SCHEMA}cusip").text
        value = infoTable.find(f"{XML_SCHEMA}value").text
        sshPrnamt = infoTable.find(
            f"{XML_SCHEMA}shrsOrPrnAmt/{XML_SCHEMA}sshPrnamt"
        ).text
        sshPrnamtType = infoTable.find(
            f"{XML_SCHEMA}shrsOrPrnAmt/{XML_SCHEMA}sshPrnamtType"
        ).text
        putCall = infoTable.find(f"{XML_SCHEMA}putCall")
        # putCall is not always present
        if putCall:
            putCall = putCall.text
        else:
            putCall = ""
        otherManager = infoTable.find(f"{XML_SCHEMA}otherManager")
        # otherManager is not always present
        if otherManager:
            otherManager = otherManager.text
        else:
            otherManager = ""
        investmentDiscretion = infoTable.find(
            f"{XML_SCHEMA}investmentDiscretion"
        ).text
        Sole = infoTable.find(
            f"{XML_SCHEMA}votingAuthority/{XML_SCHEMA}Sole"
        ).text
        Shared = infoTable.find(
            f"{XML_SCHEMA}votingAuthority/{XML_SCHEMA}Shared"
        ).text
        None_ = infoTable.find(
            f"{XML_SCHEMA}votingAuthority/{XML_SCHEMA}None"
        ).text

        data = {
            "nameOfIssuer": nameOfIssuer,
            "titleOfClass": titleOfClass,
            "cusip": cusip,
            "value": value,
            "sshPrnamt": sshPrnamt,
            "sshPrnamtType": sshPrnamtType,
            "otherManager": otherManager,
            "investmentDiscretion": investmentDiscretion,
            "putCall": putCall,
            "Sole": Sole,
            "Shared": Shared,
            "None": None_,
        }

        # holdings.append(Holding(**data))
        holdings.append(data)
    # logger.info(holdings)

    idx = 0
    # firehose records can only be batched in groups of 500
    while idx <= len(holdings):
        # send records to firehose
        batched_records = holdings[idx : idx + 500]
        logger.info(f"sending {len(batched_records)} records")
        firehose.put_record_batch(
            DeliveryStreamName=os.environ["DELIVERY_STREAM_NAME"],
            Records=[
                {'Data': json.dumps(holding)} for holding in batched_records
            ],
        )
        logger.info(f"{len(batched_records)} records sent to firehose")
        idx += 500
