import boto3
import logging
import os
import urllib.request

s3 = boto3.client('s3')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

BASE_URL = "https://www.sec.gov/Archives/"


def handler(event, context):

    # read the message
    records = event["Records"]
    for record in records:
        message_attrs = record["messageAttributes"]
        file_path = message_attrs["filename"]["stringValue"]
        filing_url = f"{BASE_URL}{file_path}"
        response = urllib.request.urlopen(filing_url)
        text = response.read().decode('utf-8')

        s3_response = s3.put_object(
            Bucket=os.environ["FILING_BUCKET"], Body=text, Key=file_path,
        )

    return {'statusCode': 200, 'body': event}


# messageAttributes: {
#     'date_filed': {
#         'stringValue': '2019-02-14',
#         'stringListValues': [],
#         'binaryListValues': [],
#         'dataType': 'String',
#     },
#     'filename': {
#         'stringValue': 'edgar/data/1000097/0001000097-19-000001.txt',
#         'stringListValues': [],
#         'binaryListValues': [],
#         'dataType': 'String',
#     },
#     'cik': {
#         'stringValue': '1000097',
#         'stringListValues': [],
#         'binaryListValues': [],
#         'dataType': 'String',
#     },
#     'company_name': {
#         'stringValue': 'KINGDON CAPITAL MANAGEMENT, L.L.C.',
#         'stringListValues': [],
#         'binaryListValues': [],
#         'dataType': 'String',
#     },
#     'form_type': {
#         'stringValue': '13F-HR',
#         'stringListValues': [],
#         'binaryListValues': [],
#         'dataType': 'String',
#     },
# }
# get the message from the event

# {
#     "Records": [
#         {
#             "messageId": "059f36b4-87a3-44ab-83d2-661975830a7d",
#             "receiptHandle": "AQEBwJnKyrHigUMZj6rYigCgxlaS3SLy0a...",
#             "body": "test",
#             "attributes": {
#                 "ApproximateReceiveCount": "1",
#                 "SentTimestamp": "1545082649183",
#                 "SenderId": "AIDAIENQZJOLO23YVJ4VO",
#                 "ApproximateFirstReceiveTimestamp": "1545082649185",
#             },
#             "messageAttributes": {},
#             "md5OfBody": "098f6bcd4621d373cade4e832627b4f6",
#             "eventSource": "aws:sqs",
#             "eventSourceARN": "arn:aws:sqs:us-east-2:123456789012:my-queue",
#             "awsRegion": "us-east-2",
#         }
#     ]
# }

# # retrieve bucket name and file_key from the S3 event
# bucket_name = event['Records'][0]['s3']['bucket']['name']
# file_key = event['Records'][0]['s3']['object']['key']
# logger.info('Reading {} from {}'.format(file_key, bucket_name))

# obj = s3.get_object(Bucket=bucket_name, Key=file_key)
# lines = obj['Body'].read().decode('utf-8').split('\n')

# Filing = namedtuple(
#     "Filing",
#     ["cik", "company_name", "form_type", "date_filed", "filename"],
# )

# # build list of 13F filing data
# filings_13f = []
# for line in lines:
#     if ".txt" in line:
#         data = line.split("|")
#         filing = Filing(*data)
#         if "13F" in filing.form_type:
#             filings_13f.append(filing)

# # build sqs messages to send in batches
# sqs_messages = [
#     {
#         "Id": f"filing_{filing_13f.cik}_{idx}",
#         "MessageBody": "filing",
#         "MessageAttributes": {
#             "cik": {"StringValue": filing_13f.cik, "DataType": "String"},
#             "company_name": {
#                 "StringValue": filing_13f.company_name,
#                 "DataType": "String",
#             },
#             "form_type": {
#                 "StringValue": filing_13f.form_type,
#                 "DataType": "String",
#             },
#             "date_filed": {
#                 "StringValue": filing_13f.date_filed,
#                 "DataType": "String",
#             },
#             "filename": {
#                 "StringValue": filing_13f.filename,
#                 "DataType": "String",
#             },
#         },
#     }
#     for idx, filing_13f in enumerate(filings_13f)
# ]

# message_count = len(sqs_messages)
# idx = 0

# # sqs messages can only be batched in groups of 10
# while idx <= message_count:
#     sqs.send_message_batch(
#         QueueUrl=os.environ["QUEUE_URL"],
#         Entries=sqs_messages[idx : idx + 10],
#     )
#     idx += 10

# return {'statusCode': 200, 'body': event}


# "nameOfIssuer"  # Name of security
# "titleOfClass"  # Type of security, e.g., class A shares
# "cusip"  # The security's CUSIP number
# "value"  # The market value of the holding in thousands
# "sshPrnamt"  # Total number of shares of the specified security
# "sshPrnamtType"  # SH = shares, PRN = principal amount
# "investmentDiscretion"  # SOLE, DFND (shared defined), or OTR (shared other)
# "putCall"  # Empty, PUT, or CALL
# "otherManager"  # Other managers on whose behalf these holdings are being reported
# "votingAuthority"  # Number of shares for which the manager exercises shared, sole, or no voting authority

