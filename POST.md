# Project overview

I have a question about a project I'm doing to learn more about serverless technologies on AWS. Here are the goals of the project:

- Collect and consolidate Quarterly 13F filing data from the SEC
- Generate a single CSV file containing all holdings from every 13F filing since 2013
- Use only serverless technologies on AWS
- Define and deploy all of the project infrastructure using AWS CDK
- Learn more about the SEC, 13F and do analytics on the collected data
- Share my experience and get feedback on serverless architecture patterns and best practices

Here's my overall approach:

1. The SEC makes available a file each quarter that contains a list of all SEC filings, including 13F filings.

   - The file is called the `Master Index of EDGAR Dissemination Feed`
   - The file contains the following fields: `CIK|Company Name|Form Type|Date Filed|Filename`
   - There are about 200,000+ filings per quarter, ~6,000 of which are 13F filings
   - This file will be uploaded to an S3 bucket (`feed_bucket`) that triggers a Lambda function (`feed_lambda`)

1. `feed_lambda` parses the Quarterly file and sends messages to an SQS Queue

   - The messages sent to SQS contain the information from each row that is a 13F filing: `CIK` (an company index), `Company Name`, `Form Type` (there are some variants to 13F filings), `Date Filed` and `Filename`
   - `Filename` is the location of the 13F filing on `https://www.sec.gov/Archives/`, for example `https://www.sec.gov/Archives/edgar/data/1000097/0001000097-19-000001.txt`.
   - SQS messages are processed by by a lambda function called `save_filing`

1. `save_filing` fetches 13F filings from `sec.gov` and saves these raw files to S3

   - The `save_filing` Lambda function process up to 10 SQS messages at a time.
   - `save_filing` saves raw `.txt` file filings to a `filings` S3 bucket that triggers a `process_filing` Lambda function.

1. `process_filing` uses regular expressions and Python's native XML parsing library to read the filing data

   - Each filing contains a list of holdings that are read into a `namedtuple`. Additional processing, cleaning and validation are done for each holding listed in the given 13F filing.
   - To save the cleaned holdings, I'm unsure of which option would the best fit. Some options I have considered are listed below:

## Options for storing 13F data

1. Store each holding as an Item in DynamoDB

   - I think that getting data in to DynamoDB woud be easy, but there's no easy way to export the full set of items from **DynamoDB** (possibly over 100 million items)
   - I haven't used DynamoDB before and I'm interested in trying it out.

1. Save holding data to CSV files stored in an S3 bucket and export the data using a `select *` query on **AWS Athena**

   - This could be a good option, but I would have lots of small files which might not be optimal for use with Athena
   - The speed of the query to export data is not super important, but the size of the dataset is nowhere close to the Peta-byte scale that Athena can handle

1. Store data in **Aurora Postgres Serverless**

   - I'm familiar with Postgres, but I'm not sure if I need it
   - This would also involve a VPC which could mean having to

As I mentioned above, my goal is to export large CSV with all 13F holding data that I can use in a Business Intelligence tool such as Google Data Studio, Tableau or Qlik.

Does anyone have suggestions on how best to store data for this project?

## Community Suggestions

1. Use Kinesis Firehose!
1. Maybe use postgres as well?

## Update

I'm using the suggestion to use Athena + S3 and to
