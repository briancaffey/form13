# Form 13 Data

This project aims to demonstrate a serverless data pipeline on AWS using the following:

- S3
- Lambda
- SQS
- Kinesis Firehose
- Athena
- QuickSight
- AWS CDK
- Python

There specific goals of this project include:

- Collecting Form 13 Filings from 2013 to 2020
- Cleaning and storing the data in Parquet format
- Explore and analyze the data using QuickSight
- Export all of the raw data as a single CSV file
