aws s3 rb s3://dev-form-13-firehose-destination-bucket --force
aws s3 rb s3://dev-form-13-filing-bucket --force
aws s3 rb s3://dev-form-13-dissemination-feed --force
aws s3 rb s3://dev-form-13-athena-queries --force