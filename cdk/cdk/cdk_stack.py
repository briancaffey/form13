from aws_cdk import (
    core,
    aws_s3,
    aws_s3_notifications,
    aws_lambda,
    aws_lambda_event_sources,
    aws_sqs,
    aws_kinesisfirehose,
    aws_iam,
    aws_glue as glue,
    aws_athena,
)


class DataSize:
    @classmethod
    def bytes(cls, size):
        return size

    @classmethod
    def kilobytes(cls, size):
        return cls.bytes(size) * 1000

    @classmethod
    def megabytes(cls, size):
        return cls.kilobytes(size) * 1000

    @classmethod
    def gigabytes(cls, size):
        return cls.megabytes(size) * 1000

    @classmethod
    def terabytes(cls, size):
        return cls.gigabytes(size) * 1000


class CdkStack(core.Stack):
    def __init__(
        self,
        scope: core.Construct,
        id: str,
        environment_name: str,
        # base_domain_name: str,
        # full_domain_name: str,
        base_app_name: str,
        full_app_name: str,
        **kwargs,
    ) -> None:
        super().__init__(scope, id, **kwargs)

        # The code that defines your stack goes here

        self.base_lambda_layer = aws_lambda.LayerVersion(
            self,
            "BaseLambdaLayer",
            code=aws_lambda.AssetCode("./lambda/layers/base"),
            compatible_runtimes=[aws_lambda.Runtime.PYTHON_3_8,],
        )

        self.dissemintation_feed_bucket = aws_s3.Bucket(
            self,
            "DisseminationFeedBucket",
            bucket_name=f"{full_app_name}-dissemination-feed",
        )

        self.filing_queue = aws_sqs.Queue(
            self,
            "FilingQueue",
            queue_name=f"{full_app_name}-filing-queue",
            visibility_timeout=core.Duration.seconds(60),
        )

        self.dissemination_feed_lambda = aws_lambda.Function(
            self,
            "DisseminationFeedLambda",
            runtime=aws_lambda.Runtime.PYTHON_3_8,
            handler="lambda-handler.handler",
            code=aws_lambda.AssetCode("./lambda/functions/dissemination_feed"),
            timeout=core.Duration.seconds(30),
            layers=[self.base_lambda_layer],
            environment={"QUEUE_URL": self.filing_queue.queue_url},
        )

        self.filing_queue.grant_send_messages(self.dissemination_feed_lambda)

        self.dissemination_feed_bucket_notification = aws_s3_notifications.LambdaDestination(
            self.dissemination_feed_lambda
        )

        self.dissemintation_feed_bucket.add_event_notification(
            aws_s3.EventType.OBJECT_CREATED,
            self.dissemination_feed_bucket_notification,
        )

        self.dissemintation_feed_bucket.grant_read(
            self.dissemination_feed_lambda
        )

        self.filing_bucket = aws_s3.Bucket(
            self, "FilingBucket", bucket_name=f"{full_app_name}-filing-bucket",
        )

        self.save_filing_lambda = aws_lambda.Function(
            self,
            "SaveFilingLambda",
            runtime=aws_lambda.Runtime.PYTHON_3_8,
            handler="lambda-handler.handler",
            code=aws_lambda.AssetCode("./lambda/functions/save_filing"),
            timeout=core.Duration.seconds(30),
            layers=[self.base_lambda_layer],
            environment={"FILING_BUCKET": self.filing_bucket.bucket_name},
        )

        self.filing_bucket.grant_read_write(self.save_filing_lambda)

        self.sqs_event_source = aws_lambda_event_sources.SqsEventSource(
            self.filing_queue, batch_size=10
        )

        self.save_filing_lambda.add_event_source(self.sqs_event_source)

        self.process_filing_lambda = aws_lambda.Function(
            self,
            "ProcessFilingLambda",
            runtime=aws_lambda.Runtime.PYTHON_3_8,
            handler="lambda-handler.handler",
            code=aws_lambda.AssetCode("./lambda/functions/process_filing"),
            timeout=core.Duration.seconds(30),
            layers=[self.base_lambda_layer],
            environment={
                "DELIVERY_STREAM_NAME": f"{full_app_name}-delivery-stream"
            },
        )

        self.process_filing_lambda_kinesis_policy = aws_iam.PolicyStatement(
            actions=["firehose:PutRecord", "firehose:PutRecordBatch"],
            resources=["*"],
        )

        self.process_filing_lambda.add_to_role_policy(
            self.process_filing_lambda_kinesis_policy
        )

        self.filing_bucket_notification = aws_s3_notifications.LambdaDestination(
            self.process_filing_lambda
        )

        self.filing_bucket.add_event_notification(
            aws_s3.EventType.OBJECT_CREATED, self.filing_bucket_notification,
        )

        self.filing_bucket.grant_read(self.process_filing_lambda)

        self.firehose_destination_bucket = aws_s3.Bucket(
            self,
            "FirehoseDestinationBucket",
            bucket_name=f"{full_app_name}-firehose-destination-bucket",
        )

        self.firehose_role = aws_iam.Role(
            self,
            "FirehoseRole",
            assumed_by=aws_iam.ServicePrincipal(
                service="firehose.amazonaws.com"
            ),
        )

        self.firehose_s3_policy_statement = aws_iam.PolicyStatement(
            actions=[
                "s3:AbortMultipartUpload",
                "s3:GetBucketLocation",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads",
                "s3:PutObject",
            ],
            resources=[
                self.firehose_destination_bucket.bucket_arn,
                f"{self.firehose_destination_bucket.bucket_arn}/*",
            ],
        )

        self.firehose_role.add_to_policy(self.firehose_s3_policy_statement)

        self.glue_filing_database_name = (
            f"{full_app_name.replace('-', '_')}_filing_database"
        )
        self.glue_database = glue.Database(
            self, "GlueDatabase", database_name=self.glue_filing_database_name,
        )

        self.glue_filing_table_name = (
            f"{full_app_name.replace('-', '_')}_filing_table"
        )
        self.filing_glue_table = glue.Table(
            self,
            "FilingGlueTable",
            database=self.glue_database,
            data_format=glue.DataFormat.PARQUET,
            # data_format=glue.DataFormat(
            #     input_format=glue.InputFormat.TEXT,
            #     output_format=glue.OutputFormat.PARQUET,
            #     serialization_library=glue.SerializationLibrary.HIVE_JSON,
            # ),
            bucket=self.firehose_destination_bucket,
            table_name=self.glue_filing_table_name,
            columns=[
                glue.Column(
                    name="nameOfIssuer",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="titleOfClass",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="cusip",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="value",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="sshPrnamt",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="sshPrnamtType",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="otherManager",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="investmentDiscretion",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="putCall",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="Sole",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="Shared",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
                glue.Column(
                    name="None",
                    type=glue.Type(input_string="string", is_primitive=False),
                ),
            ],
        )

        self.firehose_glue_role = aws_iam.Role(
            self,
            "FirehoseGlueRole",
            assumed_by=aws_iam.ServicePrincipal(
                service="firehose.amazonaws.com"
            ),
        )

        self.firehose_glue_policy_statement = aws_iam.PolicyStatement(
            actions=[
                "glue:GetTable",
                "glue:GetTableVersion",
                "glue:GetTableVersions",
            ],
            resources=["*", self.filing_glue_table.table_arn,],
        )

        self.firehose_glue_role.add_to_policy(
            self.firehose_glue_policy_statement
        )

        self.firehose_delivery_stream = aws_kinesisfirehose.CfnDeliveryStream(
            self,
            "FirehoseDeliveryStream",
            delivery_stream_name=f"{full_app_name}-delivery-stream",
            delivery_stream_type="DirectPut",
            # extended_s3_destination_configuration={}
            extended_s3_destination_configuration={
                "bucketArn": self.firehose_destination_bucket.bucket_arn,
                "bufferingHints": {"sizeInMBs": 64, "intervalInSeconds": 300,},
                # "CloudWatchLoggingOptions": CloudWatchLoggingOptions,
                # "CompressionFormat": String,
                "dataFormatConversionConfiguration": {
                    "enabled": True,
                    "inputFormatConfiguration": {
                        "deserializer": {"hiveJsonSerDe": {}}
                    },
                    "outputFormatConfiguration": {
                        "serializer": {"parquetSerDe": {}}
                    },
                    "schemaConfiguration": {
                        # "catalogId": self.env.account,
                        "databaseName": self.glue_filing_database_name,
                        "tableName": self.filing_glue_table.table_name,
                        "roleArn": self.firehose_glue_role.role_arn,
                    },
                },
                # "EncryptionConfiguration": EncryptionConfiguration,
                # "ErrorOutputPrefix": String,
                # "Prefix": String,
                # "ProcessingConfiguration": ProcessingConfiguration,
                "roleArn": self.firehose_role.role_arn,
                # "S3BackupConfiguration": S3DestinationConfiguration,
                # "S3BackupMode": String,
            },
        )

        self.firehose_delivery_stream.add_depends_on(
            self.firehose_role.node.default_child
        )

        # This bucket holds Athena query results
        self.athena_queries_bucket = aws_s3.Bucket(
            self,
            "AthenaQueryResultsBucket",
            bucket_name=f"{full_app_name}-athena-queries",
            removal_policy=core.RemovalPolicy.DESTROY,
        )

        self.athena_workgroup = aws_athena.CfnWorkGroup(
            self,
            'AthenaWorkgroup',
            name=f"{full_app_name}-athena-workgroup",
            description='Workgroup for 13F filing queries',
            recursive_delete_option=True,
            state='ENABLED',
            work_group_configuration_updates=core.CfnJson(
                self,
                'WorkGroupConfigurationUpdates',
                value={
                    'EnforceWorkGroupConfiguration': True,
                    'BytesScannedCutoffPerQuery': DataSize.gigabytes(1),
                    'PublishCloudWatchMetricsEnabled': True,
                    'ResultConfigurationUpdates': {
                        'OutputLocation': f's3://{self.athena_queries_bucket.bucket_name}/',
                    },
                },
            ),
        )
