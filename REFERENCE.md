# Form 13 Data

## About 13F

[https://www.sec.gov/divisions/investment/13ffaq.htm](https://www.sec.gov/divisions/investment/13ffaq.htm)

[https://file13f.com/form-13f](https://file13f.com/form-13f)

[https://formthirteen.com/](https://formthirteen.com/)

> **What is the difference between 13F-HR and 13F-NT?**

> 13F-HR is the 13F Holdings Report and is used when all of your applicable securities are on the report. The 13F-HR can also be the 13F Combination Report which is used when some of your applicable securities are on the report and some are on someone else’s report. 13F-NT is the 13F Notice and is used when none of your applicable securities are on the report and are on someone else’s report.

> ~~What is the **13FCONP**?~~

All 13F Filing types:

- 13FCONP
- 13FCONP/A
- 13F-E
- 13F-E/A
- 13F-HR
- 13F-HR/A
- 13F-NT
- 13F-NT/A

## 13D, 13G and 13F Differences

[https://fintel.io/b/17449-making-sense-of-the-ownership-filings---the-13d-13g-and-13f](https://fintel.io/b/17449-making-sense-of-the-ownership-filings---the-13d-13g-and-13f)

## 13F List

> This list of "Section 13(f) securities" as defined by Rule 13f-1(c) [17 CFR 240.13f-1(c)] is made available to the public pursuant to Section 13 (f) (3) of the Securities Exchange Act of 1934 [15 USC 78m(f) (3)]. It is made available for use in the preparation of reports filed with the Securities and Exchange Commission pursuant to Rule 13f-1 [17 CFR 240.13f-1] under Section 13(f) of the Securities Exchange Act of 1934. An updated list is published on a quarterly basis.

[https://www.sec.gov/divisions/investment/13flists.htm](https://www.sec.gov/divisions/investment/13flists.htm)

## EDGAR Search

[https://www.sec.gov/edgar/search/#](https://www.sec.gov/edgar/search/#)

## CIK/CUSIP Mapping

1. WRDS - [https://wrds-www.wharton.upenn.edu/pages/grid-items/wrds-sec-linking-tables/](https://wrds-www.wharton.upenn.edu/pages/grid-items/wrds-sec-linking-tables/)

1. Python Script: [https://github.com/leoliu0/cik-cusip-mapping](https://github.com/leoliu0/cik-cusip-mapping)

```py
with open(f'master.idx', 'wb') as f:
    for year in range(1994, 2023):
        for q in range(1, 5):
            print(year, q)
            content = requests.get(
                f"https://www.sec.gov/Archives/edgar/full-index/{year}/QTR{q}/master.idx"
            ).content
            f.write(content)
```

## Process

1. Download Quarterly `Master Index of EDGAR Dissemination Feed`s:

```py
url = f"https://www.sec.gov/Archives/edgar/full-index/{year}/QTR{quarter}/master.idx"
```

1. Load file into CSV
1. Filter out 13F filings
1. Download individual XML files (~6k/quarter 13F)
1. Scrape data from XML files, add meta data (Quarter, Filer CIK, etc.)
1. Save data to Postgres? ~~DynamoDB?~~

## Architecture

1. Django
1. Models

- Quarterly Feed files
- Individual Filing files (data file + meta data)
- Individual filing line item (FK to Filing file, data normalization?)

1. Django management commands for processing Quarterly and 13F filing files

- Downloading files
- Processing files

## Serverless Architecture

1. `quarterly-dissemination-feed-files` bucket
1. S3 Event Source

## CDK Examples

- [Lambda and SQS](https://github.com/aws-samples/aws-cdk-examples/blob/master/python/api-sqs-lambda/api_sqs_lambda/api_sqs_lambda_stack.py)
- [Lambda S3 Trigger example](https://github.com/aws-samples/aws-cdk-examples/blob/master/python/lambda-s3-trigger/s3trigger/s3trigger_stack.py)

## Reference

1. SQS Event Source: https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_lambda_event_sources/SqsEventSource.html

1. Kinesis Firehose + Glue Example [https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-kinesisfirehose-deliverystream.html#cfn-kinesisfirehose-deliverystream-s3destinationconfiguration](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-kinesisfirehose-deliverystream.html#cfn-kinesisfirehose-deliverystream-s3destinationconfiguration)

## Related

- Article on using SQS with Lambda: [https://data.solita.fi/lessons-learned-from-combining-sqs-and-lambda-in-a-data-project/](https://data.solita.fi/lessons-learned-from-combining-sqs-and-lambda-in-a-data-project/)

## Edge Cases

Royal Bank of Canada filing ([https://www.sec.gov/Archives/edgar/data/1000275/0001567619-19-004295.txt](https://www.sec.gov/Archives/edgar/data/1000275/0001567619-19-004295.txt)), contains 25,000 holdings and the lambda function times out while reading the file.
